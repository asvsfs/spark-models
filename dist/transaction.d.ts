import { Transaction } from "ethers";
export interface WTransaction extends Transaction {
    blockNumber: string;
    blockHash: string;
    timeStamp: number;
    gas: number;
}
