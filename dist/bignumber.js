"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
exports.default = new mongoose_1.Schema({
    _hex: { type: String },
    _isBigNumber: { type: Boolean }
});
