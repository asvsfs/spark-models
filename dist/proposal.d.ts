import { BigNumber } from "ethers";
import { ObjectId } from "mongodb";
export interface Proposal {
    _id?: ObjectId;
    calldatas: string[];
    description: string;
    startBlock: BigNumber;
    endBlock: BigNumber;
    proposer: string;
    proposalId: BigNumber;
    signatures: string[];
    targets: string[];
    txHash: string;
    hash: string;
    contractAddress: string;
}
