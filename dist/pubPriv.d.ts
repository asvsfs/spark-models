export interface PubPriv {
    pubKey: string;
    privKey: string;
}
