import { Types } from 'mongoose';
/** Feed
 * feed is a news/update from an athlete or a heads-up , feeds are just content
 */
export interface Feed {
    _id: Types.ObjectId;
    userId: string;
    text: string;
    imageUrl: string[];
    date: number;
    /**
     * link to external content related to this feed
     */
    contentLink: string;
}
