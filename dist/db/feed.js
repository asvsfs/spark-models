"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var feedSchema = new mongoose_1.Schema({
    _id: { type: mongoose_1.Types.ObjectId, required: true },
    userId: { type: String, required: true },
    text: { type: String, required: true },
    imageUrl: { type: [String] },
    date: { type: Number, required: true },
    contentLink: { type: String }
}, {
    timestamps: true
});
exports.default = (0, mongoose_1.model)('Feed', feedSchema);
