"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var bignumber_1 = require("../bignumber");
var platformStatSchema = new mongoose_1.Schema({
    _id: { type: mongoose_1.Types.ObjectId, required: true },
    date: { type: Number, required: true },
    totalUBI: { type: bignumber_1.default, required: true },
    totalStakedWND: { type: bignumber_1.default, required: true }
}, {
    timestamps: true
});
exports.default = (0, mongoose_1.model)('PlatformStat', platformStatSchema);
