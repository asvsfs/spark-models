"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var bignumber_1 = require("../bignumber");
var proposalSchema = new mongoose_1.Schema({
    _id: { type: mongoose_1.Types.ObjectId },
    calldatas: { type: [String], required: true },
    description: { type: String, required: true },
    startBlock: { type: bignumber_1.default, required: true },
    endBlock: { type: bignumber_1.default, required: true },
    proposer: { type: String, required: true },
    proposalId: { type: bignumber_1.default, required: true },
    signatures: { type: [String], required: true },
    targets: { type: [String], required: true },
    txHash: { type: String, required: true },
    hash: { type: String, required: true },
    contractAddress: { type: String, required: true }
}, {
    timestamps: true
});
exports.default = (0, mongoose_1.model)('Proposal', proposalSchema);
