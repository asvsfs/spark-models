"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var pubPrivSchema = new mongoose_1.Schema({
    pubKey: { type: String, required: true },
    privKey: { type: String, required: true }
}, {
    timestamps: true
});
exports.default = (0, mongoose_1.model)('PubPriv', pubPrivSchema);
