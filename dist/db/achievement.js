"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var achievementSchema = new mongoose_1.Schema({
    _id: { type: mongoose_1.Types.ObjectId, required: true },
    nftContract: { type: String, required: true },
    rewardXp: { type: Number, required: true },
    description: { type: String, required: true },
    link: { type: String, required: true }
}, {
    timestamps: true
});
exports.default = (0, mongoose_1.model)('Achievement', achievementSchema);
