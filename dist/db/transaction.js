"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var wTransactionSchema = new mongoose_1.Schema({
    hash: { type: String, required: true },
    nonce: { type: Number, required: true },
    blockHash: { type: String, required: true },
    blockNumber: { type: String, required: true },
    transactionIndex: { type: Number, required: true },
    from: { type: String, required: true },
    to: { type: String, required: true },
    value: { type: String, required: true },
    gasPrice: { type: String, required: true },
    gas: { type: Number, required: true },
    input: { type: String, required: true },
    timeStamp: { type: Number, required: true }
}, {
    timestamps: true
});
exports.default = (0, mongoose_1.model)('WTransaction', wTransactionSchema);
