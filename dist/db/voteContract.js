"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var topicSchema = new mongoose_1.Schema({
    hex: { type: String, required: true },
    name: { type: String, required: true }
});
var voteContractSchema = new mongoose_1.Schema({
    address: { type: String, required: true },
    topics: [topicSchema]
}, {
    timestamps: true
});
exports.default = (0, mongoose_1.model)('VoteContract', voteContractSchema);
