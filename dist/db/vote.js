"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var bignumber_1 = require("../bignumber");
var voteSchema = new mongoose_1.Schema({
    _id: { type: mongoose_1.Types.ObjectId, required: true },
    proposalId: { type: bignumber_1.default, required: true },
    reason: { type: String, required: true },
    support: { type: Number, required: true },
    weight: { type: bignumber_1.default, required: true },
    txHash: { type: String, required: true },
    contractAddress: { type: String, required: true },
    userAddress: { type: String, required: true },
    blockTimestamp: { type: Number, required: true }
}, {
    timestamps: true
});
exports.default = (0, mongoose_1.model)('Vote', voteSchema);
