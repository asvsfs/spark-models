"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var userSchema = new mongoose_1.Schema({
    _id: mongoose_1.Types.ObjectId,
    userName: { type: String, required: true },
    firstName: { type: String, required: false },
    lastName: { type: String, required: false },
    bio: { type: String, required: false },
    pubKey: { type: String, required: true },
    walletAddress: { type: String, required: true },
    data: { type: String, required: true },
    signedData: { type: String, required: true },
    email: { type: String, required: false },
    password: { type: String },
    isAthlete: { type: Boolean, required: true, immutable: true },
    totalXp: { type: Number, default: 0 },
    curLvlXp: { type: Number, default: 0 },
    nextLvlXp: { type: Number, default: 100 },
    lvl: { type: Number, default: 1 },
    title: { type: String, default: 'Beginner' },
    achievements: [{ type: mongoose_1.Types.ObjectId, ref: 'Achievement' }]
}, { timestamps: true });
var UserDB = (0, mongoose_1.model)('User', userSchema);
exports.default = UserDB;
