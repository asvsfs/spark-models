"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var voteEventSchema = new mongoose_1.Schema({
    _id: { type: mongoose_1.Types.ObjectId, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true },
    imageUrl: { type: [String] },
    contentUrl: { type: String, required: true },
    startDate: { type: Number, required: true },
    voteEndDate: { type: Number, required: true },
    proposedToken: { type: Number, required: true },
    floatProposal: { type: Boolean },
    athleteId: { type: mongoose_1.Types.ObjectId, required: true },
    contractAddress: { type: String, required: true },
    proposalId: { type: String, required: true },
    hash: { type: String, required: true },
    txHash: { type: String, required: true }
}, {
    timestamps: true
});
exports.default = (0, mongoose_1.model)('VoteEvent', voteEventSchema);
