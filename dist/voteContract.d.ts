export interface VoteContract {
    address: string;
    topics: Topic[];
}
export interface Topic {
    hex: string;
    name: string;
}
