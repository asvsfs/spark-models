import { Types } from 'mongoose';
/** Achievement NFTs */
export interface Achievement {
    _id: Types.ObjectId;
    nftContract: string;
    rewardXp: number;
    description: string;
    /** link to achievement page! */
    link: string;
}
