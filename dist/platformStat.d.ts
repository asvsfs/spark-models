import { BigNumber } from 'ethers';
import { Types } from 'mongoose';
export interface PlatformStat {
    _id: Types.ObjectId;
    date: number;
    /** UBI to be distrbuted on this date */
    totalUBI: BigNumber;
    /** Total locked Wnd on this date */
    totalStakedWND: BigNumber;
}
