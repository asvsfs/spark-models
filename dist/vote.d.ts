import { BigNumber } from "ethers";
import { Types } from "mongoose";
export interface Vote {
    _id: Types.ObjectId;
    proposalId: BigNumber;
    reason: string;
    support: number;
    weight: BigNumber;
    txHash: string;
    contractAddress: string;
    userAddress: string;
    blockTimestamp: number;
}
