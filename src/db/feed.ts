import { Schema, model, Types } from 'mongoose';
import { Feed } from '../feed';

const feedSchema = new Schema({
  _id: { type: Types.ObjectId, required: true },
  userId: { type: String, required: true },
  text: { type: String, required: true },
  imageUrl: { type: [String] },
  date: { type: Number, required: true },
  contentLink: { type: String }
}, {
  timestamps: true
});

export default model<Feed & { _id: Types.ObjectId }>('Feed', feedSchema);