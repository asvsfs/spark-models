import { Schema, model, Types } from 'mongoose';
import { WTransaction } from '../transaction';

const wTransactionSchema = new Schema({
    hash: { type: String, required: true },
    nonce: { type: Number, required: true },
    blockHash: { type: String, required: true },
    blockNumber: { type: String, required: true },
    transactionIndex: { type: Number, required: true },
    from: { type: String, required: true },
    to: { type: String, required: true },
    value: { type: String, required: true },
    gasPrice: { type: String, required: true },
    gas: { type: Number, required: true },
    input: { type: String, required: true },
    timeStamp: { type: Number, required: true }
}, {
    timestamps: true
});

export default model<WTransaction & { _id: Types.ObjectId }>('WTransaction', wTransactionSchema);