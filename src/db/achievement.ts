import { Schema, model, Types } from 'mongoose';
import { Achievement } from '../achievement';

const achievementSchema = new Schema({
  _id: { type: Types.ObjectId, required: true },
  nftContract: { type: String, required: true },
  rewardXp: { type: Number, required: true },
  description: { type: String, required: true },
  link: { type: String, required: true }
}, {
  timestamps: true
});

export default model<Achievement & { _id: Types.ObjectId }>('Achievement', achievementSchema);