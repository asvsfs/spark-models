import { Schema, model, Types } from 'mongoose';
import { VoteContract } from '../voteContract';

const topicSchema = new Schema({
    hex: { type: String, required: true },
    name: { type: String, required: true }
});

const voteContractSchema = new Schema({
    address: { type: String, required: true },
    topics: [topicSchema]
}, {
    timestamps: true
});

export default model<VoteContract & { _id: Types.ObjectId }>('VoteContract', voteContractSchema);