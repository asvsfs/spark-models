import { Schema, model, Types } from 'mongoose';
import { PubPriv } from '../pubPriv';

const pubPrivSchema = new Schema<PubPriv>({
  pubKey: { type: String, required: true },
  privKey: { type: String, required: true }
}, {
  timestamps: true
});

export default model<PubPriv & { _id: Types.ObjectId }>('PubPriv', pubPrivSchema);
