import { Schema, model, Types } from 'mongoose';
import BigNumber from '../bignumber'
import { Vote } from '../vote';

const voteSchema = new Schema({
    _id: { type: Types.ObjectId, required: true },
    proposalId: { type: BigNumber, required: true },
    reason: { type: String, required: true },
    support: { type: Number, required: true },
    weight: { type: BigNumber, required: true },
    txHash: { type: String, required: true },
    contractAddress: { type: String, required: true },
    userAddress: { type: String, required: true },
    blockTimestamp: { type: Number, required: true }
}, {
    timestamps: true
});

export default model<Vote & { _id: Types.ObjectId }>('Vote', voteSchema);