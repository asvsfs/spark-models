import { Schema, model, Types } from 'mongoose';
import { VoteEvent } from '../voteEvent';

const voteEventSchema = new Schema({
    _id: { type: Types.ObjectId, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true },
    imageUrl: { type: [String] },
    contentUrl: { type: String, required: true },
    startDate: { type: Number, required: true },
    voteEndDate: { type: Number, required: true },
    proposedToken: { type: Number, required: true },
    floatProposal: { type: Boolean },
    athleteId: { type: Types.ObjectId, required: true },
    contractAddress: { type: String, required: true },
    proposalId: { type: String, required: true },
    hash: { type: String, required: true },
    txHash: { type: String, required: true }
}, {
    timestamps: true
});

export default model<VoteEvent & { _id: Types.ObjectId }>('VoteEvent', voteEventSchema);