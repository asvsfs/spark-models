import { Schema, model, Types } from 'mongoose';
import BigNumber from '../bignumber';
import { PlatformStat } from '../platformStat';

const platformStatSchema = new Schema({
  _id: { type: Types.ObjectId, required: true },
  date: { type: Number, required: true },
  totalUBI: { type: BigNumber, required: true },
  totalStakedWND: { type: BigNumber, required: true }
}, {
  timestamps: true
});

export default model<PlatformStat & { _id: Types.ObjectId }>('PlatformStat', platformStatSchema);