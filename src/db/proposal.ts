import { Schema, model, Types } from 'mongoose';
import { ObjectId } from 'mongodb';
import BigNumber from '../bignumber'
import { Proposal } from '../proposal';

const proposalSchema = new Schema({
    _id: { type: Types.ObjectId },
    calldatas: { type: [String], required: true },
    description: { type: String, required: true },
    startBlock: { type: BigNumber, required: true },
    endBlock: { type: BigNumber, required: true },
    proposer: { type: String, required: true },
    proposalId: { type: BigNumber, required: true },
    signatures: { type: [String], required: true },
    targets: { type: [String], required: true },
    txHash: { type: String, required: true },
    hash: { type: String, required: true },
    contractAddress: { type: String, required: true }
}, {
    timestamps: true
});

export default model<Proposal & { _id: Types.ObjectId }>('Proposal', proposalSchema);
