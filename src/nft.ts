export interface NFT {
  title: string;
  id: number;
  owner: string;
  metadataURI: string;
  imageURI: string;
  metadata?: any;
}
