import { Types } from 'mongoose'
export interface User {
    _id?: Types.ObjectId
    userName: string
    firstName?: string
    lastName?: string
    /** pubKey 
     * Description >
     * user wallet pubkey, every wallet is a user! there should be a changing wallet mechansim, for userIds ? 
    */
    pubKey: string
    walletAddress: string
 
    signedData: string;
    data: string;
    email?: string
    /** password
     * Description > 
     * NOT RECOMMENDED
     * password is being used in case user didn't want to use wallet to sign in , in these cases we will be creating a wallet for user
     * in these cases we need to be responsible for buying crypto for users and users should be able to pay in fiat!
     */
    password?: string
    isAthlete: boolean

    // GAMIFICATION
    totalXp: number
    curLvlXp: number
    nextLvlXp: number
    lvl: number
    title: string
    /** Achievement ids */
    achievements?: Types.ObjectId[]

    /** bio
     * Description > 
     * biography of the athlete in HTML format
     */
    bio?: string
}
