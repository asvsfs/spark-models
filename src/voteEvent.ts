import { Types } from 'mongoose'
/** VoteEvent
 * Description >
 * VoteEvents are votings proposed by athletes
 */
export interface VoteEvent {
    _id: Types.ObjectId
    title: string
    description: string
    imageUrl: string[]
    /** contentUrl
     * Description >
     * this will be the content for users to see regarding this event, could be a link to a video / picture or more!
     */
    contentUrl: string
    /** startDate
     * Description >
     * date that this voting event will be started , notice that voting will be shown on website immediately after creation
     */
    startDate: number
    voteEndDate: number
    /** proposedToken
     * Description > 
     * number of tokens proposed to be allocated to this event
     */
    proposedToken: number
    /** floatProposal - reserved for future releases
     * Description > 
     * this kind of voting will have no fixed value and voters will decide on a value instead of an option.
     */
    floatProposal?: boolean //reserved for future
    athleteId: Types.ObjectId
    /** contractAddress
     * Description >
     * address of governance contract
     */
    contractAddress: string
    /** proposalId
     * Description >
     * proposalId from the governance contract which is the hash of title + id
     */
    proposalId: string
    /** hash
     * Description >
     * hash of title + id
     */
    hash: string
    /** txHash
     * Description >
     * hash of the tx that this voting event is created
     */
    txHash: string
}