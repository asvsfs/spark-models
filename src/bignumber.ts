import { Schema } from "mongoose";

export default new Schema({
    _hex: { type: String },
    _isBigNumber: { type: Boolean }
})
